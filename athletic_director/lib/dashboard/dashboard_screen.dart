import 'package:athletic_director/user_context_banner.dart';
import 'package:flutter/material.dart';
import 'notification_list.dart';
import 'calendar.dart';
import 'todo_list.dart';
import 'notes.dart';

class DashboardScreen extends StatelessWidget {
  final String username;
  const DashboardScreen({super.key, required this.username});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // Remove back button
        flexibleSpace: UserContextBanner(username: username),
      ),
      body: const DashboardLayout(),
    );
  }
}

class DashboardLayout extends StatelessWidget {
  const DashboardLayout({super.key});

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: const [
        NotificationList(),
        Calendar(),
        TodoList(),
        Notes(),
      ],
    );
  }
}