import 'package:flutter/material.dart';

import '../dashboard/dashboard_screen.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextField(
          controller: _usernameController,
          decoration: const InputDecoration(labelText: 'Username'),
        ),
        const SizedBox(height: 16.0),
        TextField(
          controller: _passwordController,
          decoration: const InputDecoration(labelText: 'Password'),
          obscureText: true,
        ),
        const SizedBox(height: 24.0),
        ElevatedButton(
          onPressed: () {
            // Perform login logic here
            String username = _usernameController.text;
            String password = _passwordController.text;

            // Validate credentials (you can add your validation logic here)
            if (username.isNotEmpty && password.isNotEmpty) {
              // Navigate to the next screen or perform additional actions
              // For now, let's print the credentials
              // print('Username: $username, Password: $password');
              if (username == 'test' && password == 'test-ray') {
                // Navigate to the dashboard screen
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DashboardScreen(username: username)
                  ),
                );
              }
            } else {
              // Show an error message or handle invalid credentials
              print('Invalid credentials');
            }
          },
          child: const Text('Login'),
        ),
      ],
    );
  }
}
