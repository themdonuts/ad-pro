import 'package:flutter/material.dart';

class UserContextBanner extends StatefulWidget {
  final String username;

  const UserContextBanner({super.key, required this.username});

  @override
  _UserContextBannerState createState() => _UserContextBannerState();
}

class _UserContextBannerState extends State<UserContextBanner> {
  String _selectedRole = 'Dashboard';

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue, // Customize the background color as needed
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          PopupMenuButton<String>(
            onSelected: (String value) {
              // Handle the selected value
              setState(() {
                _selectedRole = value;
                // Perform any actions based on the selected role
              });
            },
            color: Colors.blue,
            iconColor: Colors.white,
            tooltip: widget.username,
            icon: const Icon(Icons.account_circle, color: Colors.white),
            itemBuilder: (BuildContext context) {
              return <PopupMenuEntry<String>>[
                const PopupMenuItem<String>(
                  value: 'Profile',
                  textStyle: TextStyle(color: Colors.white),
                  child: Text('Profile', selectionColor: Colors.white,),
                ),
                const PopupMenuItem<String>(
                  value: 'Logout',
                  textStyle: TextStyle(color: Colors.white),
                  child: Text('Logout'),
                ),
              ];
            },
          ),
          Text(
            widget.username,
            style: const TextStyle(color: Colors.white),
          ),
          DropdownButton<String>(
            value: _selectedRole,
            icon: const Icon(Icons.arrow_drop_down, color: Colors.white),
            iconSize: 24,
            elevation: 16,
            style: const TextStyle(color: Colors.white),
            dropdownColor: Colors.blue,
            underline: Container(
              height: 2,
              color: Colors.white,
            ),
            onChanged: (String? newValue) {
              setState(() {
                _selectedRole = newValue!;
                // Perform any actions based on the selected role
              });
            },
            items: <String>['Dashboard', 'Teams', 'Coaches', 'Admin']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),

              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
